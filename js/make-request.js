$(document).ready(function() {
    $('#go').click(function () {
        $('#errordiv').text('');
        $('#result').html('');
        validate();
    });
});

function myFunction (e, url) {
    window.open(url);
}

//check data
function validate()
{
    $("#frmShorten").validate({
        errorPlacement: function(error, element) {
            $('#errordiv').text('');
            $('#result').html('');
            error.appendTo('#errordiv');
        },
        rules: {
            url: {url: true, required: true, minlength: 30}
        },
        messages: {
            url: {url:"&nbsp;Invalid URL format", required:"&nbsp;Enter URL",minlength:jQuery.validator.format("URL under {0} characters is not long")}
        }
    });

    if($("#frmShorten").valid()) {
        $('#errordiv').text('');
        post();
    } else {
        $('#result').html('');
    };
}

//save data
function post() {
    $.post("get-url.php", {
            url:$('#url').val()
        },
        function(data){
            $('#errordiv').text('');
            $("#result").html(data);
        });
}