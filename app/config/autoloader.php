<?php
error_reporting( E_ALL );
include_once("short.php");

function my_autoload($class_name) {
    $tmp = explode("\\", $class_name);
    $class_name = strtolower($tmp[count($tmp) - 1]);
    
    if (is_file('./app/controller/'.$class_name.'.php')) {
        require_once './app/controller/'.$class_name.'.php';
    } elseif (is_file('./app/model/'.$class_name.'.php')) {
        require_once './app/model/'.$class_name.'.php';
    }
}
spl_autoload_register("my_autoload");
?>