<?php
/*
* @param string $url
*/

namespace App\Controller;

class Hash
{
    protected $min_base = 10;
    protected $max_base = 36;

    protected $min = 100000000;
    protected $max = 9999999;

    /* Get converted input
     * @return string
     */
    public function encode()
    {
        $id=rand($this->min, $this->max);
        return base_convert($id, $this->min_base, $this->max_base);
    }

    /* Revert converted input
     * @param string $code
     * @return number
     */
    public function decode($code)
    {
        return base_convert($code, $this->max_base, $this->min_base);
    }
}