<?php
namespace App\Model;

use App\Controller\Db;
use App\Controller\Hash;

class ShortUrl
{
    protected $table = "short_url";
    protected $columns = ['id', 'url', 'code'];
    protected $db;

    public function __construct()
    {
        $this->db = Db::connect();
    }

    /* Get code from original URL
     * @param string $url
     * @return string
     */
    public function getCode($url) {
        $row = mysqli_fetch_array
        (
            mysqli_query($this->db, "select code from ".$this->table." where ".$this->columns[1]."='".$url."' limit 1"),
            MYSQLI_ASSOC
        );
        return $row['code'];
    }

    /* Get long url from short code
     * @param string $code
     * @return string
     */
    public function getUrl($code) {
        $row = mysqli_fetch_row
        (
            mysqli_query($this->db, "select url from ".$this->table." where ".$this->columns[2]."='".$code."' limit 1"),
            MYSQLI_ASSOC
        );
        return $row['url'];
    }

    /* insert record
     * @param string $url
     * @return int
     */
    public function add($url) {
        mysqli_query
        (
            $this->db,
            "insert into ".$this->table. " set ".$this->columns[1]. "='".$url."',".$this->columns[2]."='".(new Hash)->encode()."'"
        );
        return mysqli_insert_id($this->db);
    }

    /* update record
     * @param string $url
     * @return int
     */
    public function modify($url) {
        mysqli_query
        (
            $this->db,
            "update ".$this->table. " set ".$this->columns[2]. "='".(new Hash)->encode()." where ".$this->columns[1]."='".$url."'"
        );
        return mysqli_insert_id($this->db);
    }

    public function __destruct()
    {
        mysqli_close($this->db);
    }
}