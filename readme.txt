Task Conditions:
======================
Using this HTML please implement the following:

            1. Site-visitor (V) enters any original URL to the Input field, like
            http://anydomain/any/path/etc;
            2. V clicks submit button;
            3. Page makes AJAX-request;
            4. Short URL appears in Span element, like http://yourdomain/abCdE (don't use any
               external APIs as goo.gl etc.);
            5. V can copy short URL and repeat process with another link

            Short URL should redirect to the original link in any browser from any place and keep
            actuality forever, doesn't matter how many times application has been used after that.


            Requirements:

            1. Use PHP or Node.js;
            2. Don't use any frameworks.

            Expected result:

            1. Source code;
            2. System requirements and installation instructions on our platform, in English.


Installation Instructions:
============================
Platform: Debian Ubuntu 14.04.5 LTS, distribution - Linux Mint 17.3 Rosa
Database: MySql Ver 14.14 Distrib 5.5.54, for debian-linux-gnu
Web Server: Apache/2.4.7 (Ubuntu)
PHP version: 7.0

1) Run /test_task/dump.sql to create database and a table;

2) Add your database host, user and password by editing /test_task/app/config/short.php file;

3) Check if you have cURL extension enabled.
If not installed and/or enabled, you will get "curl_init() function not exists" error.

cUrl installation:
a) sudo apt-get install php7.0-curl
b) add extension to php.ini "extension=curl.so"
c) sudo service apache2 restart

4) Unzip and copy "test_task" folder into your web server directory;

5) Go to http://[your_host]/test_task/.


Use Case:
==========================
1) User enters a valid URL, more than 30 characters long;
2) User clicks on the button "Do!";
3) The short code is generated and stored in database along with the original URL;
4) The short-code link appears in the "Short URL" section;
5) User clicks on the short URL link;
6) User is redirected to the original URL in a new window/tab.

