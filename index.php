<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link href="./css/testtask-styles.css" rel="stylesheet">
    <title>XIAG test task</title>
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0" />
</head>
<body>
<div class="content">
    <header>URL shortener</header>
    <form role="form" method="post" action="get-url.php" id="frmShorten" name="frmShorten">
        <table>
            <tr>
                <th>Long URL</th>
                <th>Short URL</th>
            </tr>
            <tr>
                <td>
                    <input type="text" name="url" id="url" size="30" maxlength="500" autocomplete="off">
                    <input type="button" name="go" id="go" class="btn btn-primary" value="Do!"  />
                </td>
                <td id="result"></td>
            </tr>
            <tr>
                <td colspan="2" id="errordiv" style="color: red;font-weight: bold;"></td>
            </tr>
        </table>
    </form>
</div>
<script language="javascript" src="./js/jquery.js"></script>
<script language="javascript" src="./js/jquery.validate.min.js" type="text/javascript"></script>
<script language="javascript" src="./js/make-request.js" type="text/javascript"></script>
</body>
</html>